<?php
	include_once "src/views/HTMLPage.php";
	include_once "src/models/User.php"; 
	include_once "src/models/MysqlConnectionDetails.php";
	include_once "src/models/LoginValidator.php";
	
	class Authenticator {
		// @var HTMLPage Huvudvyn för applikationen.
		private $mainView; 
		//@var LoginValidator Valideringskontroll för att kolla användare mot databas.
		private $loginValidator;
		
		function __construct(HTMLPage $htmlPage, MysqlConnectionDetails $conDetails) {
			$this->mainView = $htmlPage;
			$this->loginValidator = new LoginValidator($conDetails);
		}
		
		//Kollar skickade uppgifter och loggar in en användare.
	    	public function Login() {
	    		$username = $this->mainView->GetUsername();
			$password = $this->mainView->GetPassword();
			
	    		if($username == null) {
	    				
					$this->mainView->TellUserLoginFailedNoUsername();
					return;
	    		}
				elseif ($password == null) {
					$this->mainView->TellUserLoginFailedNoPassword();
					return;
				}

	    		if(!$this->loginValidator->IsUserValid($username, $password)) {
	    			$this->mainView->TellUserInvalidCredentials();
	    		}
				else {
					$user = new User($username, $this->mainView->GetUserAgent());
					$this->mainView->SetLoggedInUser($user);
					$this->mainView->TellUserLoggedIn();
					if($this->mainView->UserClickedStayLoggedIn()) {
						$hashed = $this->loginValidator->GetHashedPasswordForCookie($password, $this->mainView->GetUserAgent());
						$expire = $this->loginValidator->GetCookieExpiry();
						$this->mainView->SetLoginCookie($username, $hashed, $expire);
						$this->loginValidator->AddCookie($username, $expire);
					}
				}
	    	}
			//Kollar cookie och loggar in användare.
			public function LoginWithCookie() {
				$cookieUsername = $this->mainView->GetUsernameFromCookie();
				$cookiePassword = $this->mainView->GetPasswordFromCookie();
				//Försvårar inloggning genom stöld av cookies genom att tvinga den obehörige att använda samma useragent.	
				$userAgent = $this->mainView->GetUserAgent();
				$authenticated = $this->loginValidator->IsCookieValid($cookieUsername, $cookiePassword, $userAgent);
				if($authenticated) {
					$user = new User($cookieUsername, $userAgent);
					$this->mainView->SetLoggedInUser($user);
					$this->mainView->TellUserLoggedInWithCookies();
				}
			}
			
			// Kör igång kontrollen och kollar loginstatus och eventuell postad info.
			public function Run() {
				if(!$this->mainView->IsUserLoggedIn()) {
					//Logga in med kaka
					if($this->mainView->DoesUserWantToLoginWithCookie()) {
						$this->LoginWithCookie();
					}
					//Logga in
					if($this->mainView->DoesUserWantToLogin()) {
						$this->Login();
					}
				}
				//Logga ut
				elseif($this->mainView->DoesUserWantToLogout()) {
						$this->mainView->Logout();
						return;
				}
				//Försvåra obehörigs användning av sessionskakan.
				$user = $this->mainView->GetUser();
				if($user != null) {
					$agent = $user->UserAgent;
					if($this->mainView->GetUserAgent() != $agent) {
						$this->mainView->Logout();
					}
				}
			}
	    }
	
?>